#include "Support/P.h"
#include "Opt.h"
#include <cmath>
using std::max;
using std::pow;


// template<class T>
// T shape( std::array<T,4> x, T i ) {
//     return x[ 0 ] * ( p3( x[ 2 ] ) - p3( max( x[ 2 ] - i, 0 ) ) ) * 4 * M_PI / 3 +
//            x[ 1 ] * ( p2( x[ 3 ] ) - p2( max( x[ 3 ] - i, 0 ) ) ) * M_PI;
// }

struct Func {
    template<class T>
    T operator()( std::array<T,1> x ) const {
        return p2( x[ 0 ] - T( 6 ) ) * p2( x[ 0 ] - T( 4 ) ); // + p2( x[ 1 ] - T( 2 ) );
    }
};



int main() {
    using O = Opt<double,1>;
    Func func;

    O opt;
    //    P( opt.run( func, { O::I{ 0., 10. }, O::I{ 0., 10. } }, { 1e-4, 1e-4 } ) );
    P( opt.run( func, { O::I{ 0., 10. } }, { 1e-4 } ) );
}
