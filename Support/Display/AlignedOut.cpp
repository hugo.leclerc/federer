#include "AlignedOut.h"

AlignedOut::AlignedOut() {

}

void AlignedOut::write_to_stream( std::ostream &os ) const {
    using std::max;

    // nb columns
    std::size_t s = 0;
    for( const Line &line : lines )
        s = max( s, line.cs.size() );

    // column sizes
    std::vector<std::size_t> ms( s, 0 );
    for( const Line &line : lines )
        for( std::size_t i = 0; i + 1 < line.cs.size(); ++i )
            ms[ i ] = max( ms[ i ], line.cs[ i ].size() );

    // write
    for( const Line &line : lines ) {
        // 1
        if ( line.bl.size() ) {
            os << line.bl;
            if ( line.cs.size() ) {
                if ( line.bl.size() < 4 )
                    os << std::string( 4 - line.bl.size(), ' ' );
                else
                    os << "\n    ";
            }
        } else if ( line.cs.size() )
            os << "    ";

        // 2
        for( std::size_t i = 0; i < line.cs.size(); ++i ) {
            if ( i )
                os << std::string( ms[ i - 1 ] - line.cs[ i - 1 ].size(), ' ' );
            os << line.cs[ i ];
        }
        os << '\n';
    }
}

void AlignedOut::operator<<(const AlignedOut &aa) {
    for( const Line &l : aa.lines )
        lines.push_back( l );
}

AlignedOut::Push AlignedOut::operator<<( std::string str ) {
    lines.push_back( { str, {} } );
    return { &lines.back() };
}
