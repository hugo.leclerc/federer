#pragma once

#include <ostream>

struct HexRepr {
    const std::uint8_t *beg;
    const std::uint8_t *end;
};

inline
std::ostream &operator<<( std::ostream &os, const HexRepr &val ) {
    int cpt = 0;
    static const char* c = "0123456789abcdef";
    for( const std::uint8_t *ptr = val.beg; ptr < val.end; ++ptr )
        os << ( cpt++ ? " " : "" ) << c[ *ptr / 16 ] << c[ *ptr % 16 ];
    return os;
}

inline
HexRepr hex_repr( const void *beg, const void *end ) {
    return { static_cast<const std::uint8_t *>( beg ), static_cast<const std::uint8_t *>( end ) };
}

