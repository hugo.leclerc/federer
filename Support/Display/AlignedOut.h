#pragma once

#include <ostream>
#include <vector>
#include <string>

/**
*/
class AlignedOut {
public:
    struct            Line           { std::string bl; std::vector<std::string> cs; };
    struct            Push           { Push &operator<<( std::string str ) { line->cs.push_back( str ); return *this; } Line *line; };

    /**/              AlignedOut     ();

    void              write_to_stream( std::ostream &os ) const;
    void              operator<<     ( const AlignedOut &aa );
    Push              operator<<     ( std::string str );

private:
    std::vector<Line> lines;
};
