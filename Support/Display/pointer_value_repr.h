#pragma once

#include <ostream>

template<class T>
struct PointedValueRepr {
    const T &val;
};

template<class T>
std::ostream &operator<<( std::ostream &os, const PointedValueRepr<T> &p ) {
    std::size_t s = 0;
    for( const auto &v : p.val )
        os << ( s++ ? "," : "" ) << *v;
    return os;
}

template<class T>
PointedValueRepr<T> pointed_value_repr( const T &val ) {
    return { val };
}

