#pragma once

#if __cplusplus >= 201703L
#include <string_view>
using StringView = std::string_view;
#else
#include <experimental/string_view>
using StringView = std::experimental::string_view;
#endif
