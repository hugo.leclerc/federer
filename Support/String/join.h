#include <sstream>
#include <vector>

/// join with a map
template<class T,class F>
static std::string join( const std::vector<T> &lst, const std::string &sep, const F &f ) {
    if ( lst.empty() )
        return {};
    std::ostringstream ss;
    ss << f( lst[ 0 ] );
    for( size_t i = 1; i < lst.size(); ++i )
        ss << sep << f( lst[ i ] );
    return ss.str();
}

template<class T>
static std::string join( const std::vector<T> &lst, const std::string &sep = " " ) {
    return join( lst, sep, []( auto s ) { return s; } );
}
