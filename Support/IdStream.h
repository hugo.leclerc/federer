#pragma once

#include <sstream>

/**
*/
struct IdStream {
    template<class T>
    IdStream &operator<<( const T &v ) {
        std::ostringstream ss;
        ss << v;

        os << ss.str().size() << "_" << ss.str();

        return *this;
    }

    std::ostream &os;
};
