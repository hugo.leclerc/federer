#include "Support/P.h"
#include <limits>
#include "Opt.h"

template<class T, int n>
bool Opt<T,n>::S::touches( const S &s ) const {
    for( int i = 0; i < n; ++i )
        if ( inp[ i ].touches( s.inp[ i ] ) == false )
            return false;
    return true;
}

template<class T, int n>
void Opt<T,n>::S::expand( const S &s ) {
    for( int i = 0; i < n; ++i )
        inp[ i ].expand( s.inp[ i ] );
    out.expand( s.out );
}

template<class T, int n>
void Opt<T,n>::S::write_to_stream( std::ostream &os ) const {
    //    os << "(" << inp << ")=>" << out;
    os << inp << " \t" << out << "\n";
}

template<class T,int n> template<class F>
std::vector<typename Opt<T,n>::S> Opt<T,n>::run( const F &func, P inp, const std::array<T,n> &precision ) {
    using std::min;
    using std::max;

    intervals.push_back( { inp, func( inp ) } );

    while ( true ) {
        // divide intervals
        std::vector<S> oi = std::move( intervals );
        for( const S &s : oi ) {
            int best_i = 0;
            for( int i = 1; i < n; i++ )
                if ( s.inp[ best_i ].w() / precision[ best_i ] < s.inp[ i ].w() / precision[ i ] )
                    best_i = i;

            T mid = ( s.inp[ best_i ].beg + s.inp[ best_i ].end ) / 2;

            P ninp = s.inp;
            ninp[ best_i ].end = mid;
            intervals.push_back( { ninp, func( ninp ) } );

            ninp[ best_i ].beg = mid;
            ninp[ best_i ].end = s.inp[ best_i ].end;
            intervals.push_back( { ninp, func( ninp ) } );
        }

        // keep only intervals that may contain the solution
        T best_end = std::numeric_limits<T>::max();
        for( const S &s : oi )
            best_end = min( best_end, s.out.end );

        oi = std::move( intervals );
        for( S s : oi )
            if ( s.out.beg < best_end || s.out.end == best_end )
                intervals.push_back( s );

        // => gather touching boxes
        if ( valid( intervals[ 0 ].inp, precision  ) == false )
            continue;
        std::vector<S> ts = touching_sets();
        P( intervals.size(), ts.size() );

        for( int i = 0; ; i++ ) {
            if ( i == ts.size() )
                return ts;
            if ( valid( ts[ i ].inp, precision ) == false )
                break;
        }
    }
}

template<class T, int n>
bool Opt<T,n>::valid( const P &inp, const std::array<T, n> &precision ) {
    for( std::size_t i = 0; i < inp.size(); ++i )
        if ( inp[ i ].w() > precision[ i ] )
            return false;
    return true;
}


template<class T,int n>
std::vector<typename Opt<T,n>::S> Opt<T,n>::touching_sets() {
    std::vector<S> res = { intervals.back() };
    std::vector<S> oi = intervals;
    oi.pop_back();
    if ( oi.empty() )
        return res;

    // try to find a connected element. TODO: optimize
    for( std::size_t i = 0; ; ) {
        // if nothing is connected, take a new seed
        if ( i == oi.size() ) {
            res.push_back( oi.back() );
            oi.pop_back();
            if ( oi.empty() )
                return res;
            i = 0;
            continue;
        }

        // if connected
        if ( res.back().touches( oi[ i ] ) ) {
            res.back().expand( oi[ i ] );
            oi[ i ] = oi.back();
            oi.pop_back();
            if ( oi.empty() )
                return res;
            i = 0;
            continue;
        }

        ++i;
    }
}
