#ifndef OPT_H
#define OPT_H

#include "Interval.h"
#include <vector>
#include <array>

/***/
template<class T,int n>
struct Opt {
    using          I            = Interval<T>;
    using          P            = std::array<I,n>;
    struct         S            { bool touches( const S &i ) const; void expand( const S &i ); void write_to_stream( std::ostream &os ) const; P inp; I out; };

    /**/           Opt          () {}

    template       <class F>
    std::vector<S> run          ( const F &func, P inp, const std::array<T,n> &precision );

    std::vector<S> touching_sets();
    bool           valid        ( const P &inp, const std::array<T,n> &precision );

    std::vector<S> intervals;
};

#include "Opt.tcc"

#endif // OPT_H

