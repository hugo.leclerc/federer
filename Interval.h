#ifndef INTERVAL_H
#define INTERVAL_H

#include <ostream>

/***/
template<class T>
struct Interval {
    /**/ Interval       ( T beg, T end ) : beg( beg ), end( end ) {}
    /**/ Interval       ( T beg ) : beg( beg ), end( beg ) {}

    void write_to_stream( std::ostream &os ) const;
    bool touches        ( const Interval &i ) const;
    void expand         ( const Interval &i );
    T    w              () const;

    T    beg, end;
};

#include "Interval.tcc"

#endif // INTERVAL_H


