#include "Interval.h"

template<class T>
void Interval<T>::write_to_stream( std::ostream &os ) const {
    os << "[" << beg << "," << end << "]";
}

template<class T>
bool Interval<T>::touches( const Interval &i ) const {
    return end >= i.beg && beg <= i.end;
}

template<class T>
void Interval<T>::expand( const Interval &i ) {
    using std::min;
    using std::max;
    beg = min( beg, i.beg );
    end = max( end, i.end );
}

template<class T>
T Interval<T>::Interval::w() const {
    return end - beg;
}


template<class T>
Interval<T> operator+( Interval<T> a, Interval<T> b ) {
    return { a.beg + b.beg, a.end + b.end };
}

template<class T>
Interval<T> operator-( Interval<T> a, Interval<T> b ) {
    return { a.beg - b.end, a.end - b.beg };
}

template<class T>
Interval<T> operator*( Interval<T> a, Interval<T> b ) {
    return { ... };
}

template<class T>
Interval<T> max( Interval<T> a, Interval<T> b ) {
    using std::max;
    return { max( a.beg, b.beg ), max( a.end, b.end ) };
}

template<class T> T p3( T a ) {
    return a * a * a;
}

template<class T> T p2( T a ) {
    return a * a;
}

template<class T> Interval<T> p3( Interval<T> a ) {
    return { p3( a.beg ), p3( a.end ) };
}

template<class T> Interval<T> p2( Interval<T> a ) {
    if ( a.beg >= 0 )
        return { p2( a.beg ), p2( a.end ) };
    if ( a.end <= 0 )
        return { p2( a.end ), p2( a.beg ) };
    using std::max;
    return { T( 0 ), max( p2( a.beg ), p2( a.end ) ) };
}

